# jq in Docker

Used för remote script where jq is not avaliable

Using this container as the jq command, using the **-rm** throws the container when used

`docker inspect container | docker run -i --rm registry.gitlab.com/mrflutters/jq-docker .[0]`