FROM alpine:3.9.4

LABEL maintainer="MrFlutters <DeriousHD@gmail.com>"

RUN apk update && apk upgrade && \
    apk add jq && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/usr/bin/jq"]
